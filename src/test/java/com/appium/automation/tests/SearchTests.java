package com.appium.automation.tests;

import com.appium.automation.lib.CoreTestCase;
import com.appium.automation.steps.ArticleSteps;
import com.appium.automation.steps.BookmarkSteps;
import com.appium.automation.steps.MainPageSteps;
import com.appium.automation.steps.SearchSteps;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SearchTests extends CoreTestCase {

    private SearchSteps searchSteps;
    private ArticleSteps articleSteps;
    private MainPageSteps mainPageSteps;
    private BookmarkSteps bookmarkSteps;

    private static final String SEARCH_WORD = "Java";
    private static final String DEFAULT_ANDROID_SEARCH_INPUT_TEXT = "Search…";
    private static final String DEFAULT_IOS_SEARCH_INPUT_TEXT = "Search…";

    private static final String FIRST_SEARCH_RESULT_TITLE = "Java (programming language)";
    private static final String SECOND_SEARCH_RESULT_TITLE = "Java version history";
    private static final String THIRD_SEARCH_RESULT_TITLE = "Javanese language";
    private static final String FIRST_SEARCH_RESULT_DESCRIPTION = "Object-oriented programming language";
    private static final String SECOND_SEARCH_RESULT_DESCRIPTION = "Wikimedia list article";
    private static final String THIRD_SEARCH_RESULT_DESCRIPTION = "Austronesian language";

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        mainPageSteps = new MainPageSteps(getDriver());
        searchSteps = new SearchSteps(getDriver());
        articleSteps = new ArticleSteps(getDriver());
        bookmarkSteps = new BookmarkSteps(getDriver());
    }

    @Test
    public void testSearchOfFirstThreeArticles() {
        searchSteps.searchFor(SEARCH_WORD)
                .assertArticleWithCertainTitleAndDescriptionDisplayed(FIRST_SEARCH_RESULT_TITLE, FIRST_SEARCH_RESULT_DESCRIPTION)
                .assertArticleWithCertainTitleAndDescriptionDisplayed(SECOND_SEARCH_RESULT_TITLE, SECOND_SEARCH_RESULT_DESCRIPTION)
                .assertArticleWithCertainTitleAndDescriptionDisplayed(THIRD_SEARCH_RESULT_TITLE, THIRD_SEARCH_RESULT_DESCRIPTION);
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

}
