package com.appium.automation.tests;

import com.appium.automation.lib.CoreTestCase;
import com.appium.automation.steps.ArticleSteps;
import com.appium.automation.steps.SearchSteps;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FindArticleTests extends CoreTestCase {

    private static final String FIRST_ARTICLE_SEARCH_STRING = "Chromium";
    private static final String FIRST_ARTICLE_TITLE = "Chromium toxicity";

    private SearchSteps searchSteps;
    private ArticleSteps articleSteps;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        searchSteps = new SearchSteps(getDriver());
        articleSteps = new ArticleSteps(getDriver());
    }

    @Test
    public void testOpenArticleDefaultOrientation() {
        searchSteps.searchFor(FIRST_ARTICLE_SEARCH_STRING)
                .assertSearchResultSetContainsArticle(FIRST_ARTICLE_TITLE)
                .openArticle(FIRST_ARTICLE_TITLE);
        articleSteps
                .assertArticleTitle(FIRST_ARTICLE_TITLE);
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

}
