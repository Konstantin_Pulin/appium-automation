package com.appium.automation.tests;

import com.appium.automation.lib.CoreTestCase;
import com.appium.automation.steps.ArticleSteps;
import com.appium.automation.steps.BookmarkSteps;
import com.appium.automation.steps.MainPageSteps;
import com.appium.automation.steps.SearchSteps;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SaveArticlesTests extends CoreTestCase {

    private static final String FIRST_ARTICLE_SEARCH_STRING = "Chromium";
    private static final String FIRST_ARTICLE_TITLE = "Chromium toxicity";
    private static final String SECOND_ARTICLE_SEARCH_STRING = "Fight Club";
    private static final String SECOND_ARTICLE_TITLE = "Fight Club (novel)";
    private static final String ARTICLES_LIST_NAME = "My favorite articles";

    private SearchSteps searchSteps;
    private ArticleSteps articleSteps;
    private MainPageSteps mainPageSteps;
    private BookmarkSteps bookmarkSteps;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        mainPageSteps = new MainPageSteps(getDriver());
        searchSteps = new SearchSteps(getDriver());
        articleSteps = new ArticleSteps(getDriver());
        bookmarkSteps = new BookmarkSteps(getDriver());
    }

    @Test
    public void testSaveTwoArticles() {
        searchSteps.searchFor(FIRST_ARTICLE_SEARCH_STRING)
                .assertSearchResultSetContainsArticle(FIRST_ARTICLE_TITLE)
                .openArticle(FIRST_ARTICLE_TITLE);
        articleSteps.
                addArticleToReadingList(ARTICLES_LIST_NAME, FIRST_ARTICLE_TITLE)
                .closeOpenedArticle();

        searchSteps.searchFor(SECOND_ARTICLE_SEARCH_STRING)
                .assertSearchResultSetContainsArticle(SECOND_ARTICLE_TITLE)
                .openArticle(SECOND_ARTICLE_TITLE);
        articleSteps
                .addArticleToReadingList(ARTICLES_LIST_NAME, SECOND_ARTICLE_TITLE)
                .closeOpenedArticle();
        mainPageSteps.openBookmarksTab();

        //and another one way to test that article was deleted
        int countBeforeDeletion = bookmarkSteps.openBookmarksList(ARTICLES_LIST_NAME)
                .assertArticlesDisplayed(FIRST_ARTICLE_TITLE, SECOND_ARTICLE_TITLE)
                .getSavedArticlesCount();

        bookmarkSteps.deleteArticle(SECOND_ARTICLE_TITLE)
                .assertArticlesDisplayed(FIRST_ARTICLE_TITLE)
                .assertArticlesCountAfterDeletion(countBeforeDeletion)
                .openArticle(FIRST_ARTICLE_TITLE);
        articleSteps.assertArticleTitle(FIRST_ARTICLE_TITLE);
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

}
