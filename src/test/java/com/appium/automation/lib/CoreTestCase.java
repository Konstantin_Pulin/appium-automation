package com.appium.automation.lib;

import com.appium.automation.steps.LoginSteps;
import io.appium.java_client.AppiumDriver;
import lombok.Getter;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.remote.RemoteWebDriver;

import static com.appium.automation.lib.Platform.isMV;

public class CoreTestCase {

    @Getter
    private RemoteWebDriver driver;
    @Getter
    protected Platform platform;

    public void setUp() throws Exception {
        this.platform = Platform.INSTANCE;
        this.driver = platform.prepareDriver();
        if (driver instanceof AppiumDriver) {
            if (((AppiumDriver) driver).getOrientation().equals(ScreenOrientation.LANDSCAPE)) {
                rotatePortrait();
            }
        } else {
            System.out.println("It is not Appium Driver instance. Screen can not be rotated.");
        }

        if (isMV()) {
            driver.navigate().to("https://en.m.wikipedia.org/w/index.php?title=Special:UserLogin");
            LoginSteps loginSteps = new LoginSteps(driver);
            loginSteps.login();
        }
    }

    public void tearDown() throws Exception {
        driver.quit();
    }

    private void rotate(ScreenOrientation screenOrientation) {
        if (driver instanceof AppiumDriver) {
            ((AppiumDriver) driver).rotate(screenOrientation);
        }
    }

    protected void rotateLandscape() {
        rotate(ScreenOrientation.LANDSCAPE);
    }

    protected void rotatePortrait() {
        rotate(ScreenOrientation.PORTRAIT);
    }

}
