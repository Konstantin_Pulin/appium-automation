package com.appium.automation.lib;

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * Singleton enum class
 */
public enum Platform {

    INSTANCE;

    private static final String IOS = "iOS";
    private static final String ANDROID = "android";
    private static final String MOBILE_WEB = "MV";
    private static final String PLATFORM_NAME = "platformName";
    private static final String APPIUM_URL = "http://127.0.0.1:4723/wd/hub";
    private static final String CHROME_VERSION = "72.0";

    public RemoteWebDriver prepareDriver() throws MalformedURLException {
        return prepareDriver(setupCapabilities());
    }

    private RemoteWebDriver prepareDriver(DesiredCapabilities capabilities) throws MalformedURLException {
        RemoteWebDriver driver;
        if (isAndroid()) {
            driver = prepareAndroidDriver(capabilities);
        } else if (isIOs()) {
            driver = prepareIOSDriver(capabilities);
        } else if (isMV()) {
            WebDriverManager.chromedriver().version(CHROME_VERSION).setup();
            driver = new ChromeDriver(getMWChromeOptions());
        } else {
            throw new RuntimeException("Can not create mobile driver!");
        }
        return driver;
    }

    private IOSDriver prepareIOSDriver(DesiredCapabilities capabilities) throws MalformedURLException {
        return new IOSDriver(new URL(APPIUM_URL), capabilities);
    }

    private AppiumDriver prepareAndroidDriver(DesiredCapabilities capabilities) throws MalformedURLException {
        return new AndroidDriver(new URL(APPIUM_URL), capabilities);
    }

    private ChromeOptions getMWChromeOptions() {
        Map<String, Object> deviceMetrics
                = ImmutableMap.of("width", "360",
                "height", "640",
                "pixelRatio", "3.0");

        Map<String, Object> mobileEmulation
                = ImmutableMap.of("deviceMetrics", deviceMetrics,
                "userAgent", "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) ");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setCapability("mobileEmulation", mobileEmulation);
        chromeOptions.addArguments("window-size=360,640");
        return chromeOptions;
    }

    private DesiredCapabilities setupCapabilities() {
        DesiredCapabilities capabilities;
        if (isAndroid()) {
            capabilities = prepareAndroidCapabilities();
        } else if (isIOs()) {
            capabilities = prepareIOSCapabilities();
        } else if (isMV()) {
            capabilities = DesiredCapabilities.chrome();
            capabilities.setCapability(ChromeOptions.CAPABILITY, getMWChromeOptions());
        } else {
            throw new RuntimeException("Wrong mobile platform name!");
        }
        return capabilities;
    }

    public static boolean isAndroid() {
        return isPlatform(ANDROID);
    }

    public static boolean isIOs() {
        return isPlatform(IOS);
    }

    public static boolean isMV() {
        return isPlatform(MOBILE_WEB);
    }

    private static boolean isPlatform(String platform) {
        String platformName = System.getProperty(PLATFORM_NAME);
        return platformName.equalsIgnoreCase(platform);
    }

    private DesiredCapabilities prepareAndroidCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "AndroidTestDevice");
        capabilities.setCapability("platformVersion", "6.0");
        capabilities.setCapability("automationName", "Appium");
        capabilities.setCapability("appPackage", "org.wikipedia");
        capabilities.setCapability("appActivity", ".main.MainActivity");
        capabilities.setCapability("app", "/Users/konstantinpulin/appium-automation/apks/org.wikipedia.apk");
        return capabilities;
    }

    private DesiredCapabilities prepareIOSCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("deviceName", "iPhone SE");
        capabilities.setCapability("platformVersion", "11.0");
        capabilities.setCapability("app", "/Users/konstantinpulin/Downloads/Wikipedia.app");
        capabilities.setCapability("automationName", "XCUITest");
        return capabilities;
    }

}
