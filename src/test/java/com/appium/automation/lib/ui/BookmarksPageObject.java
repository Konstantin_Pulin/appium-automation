package com.appium.automation.lib.ui;

import com.appium.automation.lib.ByType;
import com.appium.automation.lib.ParametrizedSelector;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static com.appium.automation.lib.Platform.isAndroid;
import static com.appium.automation.lib.Platform.isIOs;
import static com.appium.automation.lib.Platform.isMV;

public class BookmarksPageObject extends CommonWebDriverMethods {

    @AndroidFindBy(id = "org.wikipedia:id/item_container")
    @iOSFindBy(xpath = "//XCUIElementTypeApplication[@name='Wikipedia']/XCUIElementTypeWindow[1]//XCUIElementTypeCell")
    private List<WebElement> bookMarksContainers;

    @AndroidFindBy(id = "org.wikipedia:id/item_title")
    private WebElement favoriteArticlesPageTitle;

    @FindBy(css = "li h3")
    @AndroidFindBy(id = "org.wikipedia:id/page_list_item_title")
    @iOSFindBy(xpath = "//XCUIElementTypeApplication[@name='Wikipedia']/XCUIElementTypeWindow[1]//XCUIElementTypeLink")
    private List<WebElement> articlesTitles;

    @AndroidFindBy(id = "org.wikipedia:id/reading_list_item_remove_text")
    private WebElement deleteBookmarkMenuItem;

    @iOSFindBy(id = "Reading lists")
    private WebElement readingListsTab;

    @iOSFindBy(id = "Edit")
    private WebElement iOSEditButton;

    @iOSFindBy(id = "Remove")
    private WebElement iOSRemoveButton;

    //    Templates section
    private static final ParametrizedSelector TMPL_XPATH_IOS_BOOKMARK_LIST_MENU_BUTTON = new ParametrizedSelector("//XCUIElementTypeLink[contains(@name, ''{0}'')]", ByType.ByXpath),
            TMPL_IOS_ATRICLE_TITLE = new ParametrizedSelector("//XCUIElementTypeLink[contains(@name, ''{0}'')]", ByType.ByXpath),
            TMPL_ANDROID_XPATH_BOOKMARKS_LIST_BY_TITLE_TEXT = new ParametrizedSelector("//*[@text=''{0}'']", ByType.ByXpath),
            TMPL_IOS_ID_BOOKMARKS_LIST_BY_TITLE_TEXT = new ParametrizedSelector("{0}", ByType.ById),
            TMPL_ANDROID_XPATH_ARTICLE_BY_TITLE_TEXT = new ParametrizedSelector("//*[@text=''{0}'']", ByType.ByXpath),
            TMPL_MV_XPATH_ARTICLE_BY_TITLE_TEXT = new ParametrizedSelector("//li/a/h3[text()[contains(.,''{0}'')]]", ByType.ByXpath),
            TMPL_ANDROID_XPATH_ANDROID_BOOKMARK_LIST_MENU_BUTTON =
                    new ParametrizedSelector("//*[@resource-id=''org.wikipedia:id/page_list_item_title'' and @text=''{0}'']/../../*[@resource-id=''org.wikipedia:id/page_list_item_action_primary'']", ByType.ByXpath),
            TMPL_MV_XPATH_ANDROID_BOOKMARK_LIST_MENU_BUTTON =
                    new ParametrizedSelector("//*[text()[contains(.,''{0}'')]]/../../div", ByType.ByXpath);

    public BookmarksPageObject(RemoteWebDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public List<WebElement> getArticlesTitles() {
        return waitForElements(articlesTitles, 10,
                "Error! Stored articles are not displayed!");
    }

    public List<WebElement> getBookmarksContainers() {
        return waitForElements(bookMarksContainers,
                "Error! Bookmark lists are not displayed!");
    }

    public void openBookmarkList(String listName) {
        String myBookmarksError = "Error! Can not find may favorite folder!";
        if (isAndroid()) {
            waitForElement(TMPL_ANDROID_XPATH_BOOKMARKS_LIST_BY_TITLE_TEXT.applyParameters(listName), myBookmarksError).click();
        } else {
            waitForElement(TMPL_IOS_ID_BOOKMARKS_LIST_BY_TITLE_TEXT.applyParameters(listName), myBookmarksError).click();
        }
    }

    public WebElement getFavoriteArticlesPageTitle() {
        return waitForElement(favoriteArticlesPageTitle,
                "Error! Can not find  favorite articles title!");
    }

    public void deleteArticle(String title) {
        String errorNoRemoveButtonFound = "Error! Can not find 'Remove from articles' button!";
        String errorNoOpenMenuButton = "Error! Can not find expected open menu button!";
        if (isAndroid()) {
            waitForElement(TMPL_ANDROID_XPATH_ANDROID_BOOKMARK_LIST_MENU_BUTTON.applyParameters(title),
                    errorNoOpenMenuButton).click();
            waitForElement(deleteBookmarkMenuItem,
                    errorNoRemoveButtonFound).click();
        } else if (isIOs()) {
            waitForElement(iOSEditButton, "Error! Can not find 'Edit' button!").click();
            waitForElement(TMPL_XPATH_IOS_BOOKMARK_LIST_MENU_BUTTON.applyParameters(title),
                    errorNoOpenMenuButton).click();
            waitForElement(iOSRemoveButton, errorNoRemoveButtonFound).click();
        } else if (isMV()) {
            waitForElement(TMPL_MV_XPATH_ANDROID_BOOKMARK_LIST_MENU_BUTTON.applyParameters(title),
                    errorNoRemoveButtonFound).click();
            driver.navigate().refresh();
        }
    }

    public void openArticle(String title) {
        String errorMessage = "Error! Can not open article with specified title: " + title;
        if (isAndroid()) {
            waitForElement(TMPL_ANDROID_XPATH_ARTICLE_BY_TITLE_TEXT.applyParameters(title),
                    errorMessage).click();
        } else if (isIOs()) {
            waitForElement(TMPL_IOS_ATRICLE_TITLE.applyParameters(title),
                    errorMessage).click();
        } else if (isMV()) {
            waitForElement(TMPL_MV_XPATH_ARTICLE_BY_TITLE_TEXT.applyParameters(title),
                    errorMessage).click();
        }
    }

    public void getToIosReadingList() {
        waitForElement(readingListsTab, "Error! Can not open'Reading lists tab'!").click();
    }

}
