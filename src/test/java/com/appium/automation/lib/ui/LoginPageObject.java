package com.appium.automation.lib.ui;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class LoginPageObject extends CommonWebDriverMethods {

    @FindBy(id = "wpName1")
    private WebElement userName;
    @FindBy(id = "wpPassword1")
    private WebElement password;
    @FindBy(id = "wpLoginAttempt")
    private WebElement logInBtn;

    public LoginPageObject(RemoteWebDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void login(String login, String pwd) {
        userName.sendKeys(login);
        password.sendKeys(pwd);
        logInBtn.click();
    }
}
