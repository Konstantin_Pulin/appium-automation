package com.appium.automation.lib.ui;

import com.appium.automation.lib.ByType;
import com.appium.automation.lib.ParametrizedSelector;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.appium.automation.lib.Platform.isAndroid;
import static com.appium.automation.lib.Platform.isMV;

public class ArticlePageObject extends CommonWebDriverMethods {

    @FindBy(tagName = "h1")
    @AndroidFindBy(id = "org.wikipedia:id/view_page_title_text")
    private WebElement title;

    @iOSFindBy(id = "Add")
    private WebElement addToReadingListCross;

    @FindBy(id = "ca-watch")
    @iOSFindBy(id = "Save for later")
    @AndroidFindBy(xpath = "//*[@class='android.widget.ImageView' and @content-desc='Add this article to a reading list']")
    private WebElement addToBookmarksButton;

    @FindBy(css = "li#page-actions-watch .watched")
    @iOSFindBy(id = "Saved. Activate to unsave.")
    private WebElement unsaveBookmarkButton;

    @AndroidFindBy(xpath = "//*[@class='android.widget.TextView'][@text='Got it']")
    private WebElement gotItButton;

    @iOSFindBy(xpath = "//*[@value='reading list title']")
    @AndroidFindBy(id = "org.wikipedia:id/text_input")
    private WebElement newBookmarkInput;

    @iOSFindBy(id = "Create reading list")
    @AndroidFindBy(xpath = "//*[@class='android.widget.Button'][@text='OK']")
    private WebElement submitNewBookmarkButton;

    @iOSFindBy(id = "Back")
    @AndroidFindBy(xpath = "//*[@class='android.widget.ImageButton']")
    private WebElement closeArticleButton;

    @iOSFindBy(id = "places auth close")
    private WebElement closeSyncDialogButton;

    @FindBy(xpath = ".mw-notification-area > div")
    private WebElement notification;

    //    Templates section
    private static final ParametrizedSelector TMPL_IOS_ID_ARTICLE_BY_TITLE_TEXT = new ParametrizedSelector("{0}", ByType.ById),
            TMPL_IOS_ADD_ARTICLE_ELEMENT = new ParametrizedSelector("Add “{0}” to a reading list?", ByType.ById),
            TMPL_XPATH_ANDROID_ARTICLE_TITLE_WITH_SPECIFIED_TEXT = new ParametrizedSelector("//*[@resource-id='org.wikipedia:id/view_page_title_text' and @text=''{0}'']", ByType.ByXpath),
            TMPL_XPATH__ANDROIDBOOKMARKS_LIST_BY_TITLE_TEXT = new ParametrizedSelector("//*[@text=''{0}'']", ByType.ByXpath);


    public ArticlePageObject(RemoteWebDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void waitIOSArticleTitleDisplayed(String articleTitle) {
        waitForElement(TMPL_IOS_ID_ARTICLE_BY_TITLE_TEXT.applyParameters(articleTitle),
                "Error! Article title is not displayed!");
    }

    public void checkOpenedArticleTitlePromptly(String articleTitle) {
        String errorMessage = "Error! Article title is not displayed!";
        if (isAndroid()) {
            assertElementPresent(TMPL_XPATH_ANDROID_ARTICLE_TITLE_WITH_SPECIFIED_TEXT.applyParameters(articleTitle),
                    errorMessage);
        } else {
            assertElementPresent(TMPL_IOS_ID_ARTICLE_BY_TITLE_TEXT.applyParameters(articleTitle),
                    errorMessage);
        }
    }

    public WebElement getArticleTitleElement() {
        return waitForElement(title,
                "Error! Can not find article title!");
    }

    public String getArticleTitleText() {
        return waitForElement(title,
                "Error! Can not find article title!").getText();
    }

    public boolean isArticleSavedToList() {
        return waitForElement(addToBookmarksButton, "Error! Con not find add to bookmarks button!")
                .getAttribute("class").contains("watched");
    }

    public void clickAddToBookmark() {
        waitForElement(addToBookmarksButton,
                "Error! Can not find more options button!").click();
        if (isMV()) {
            waitForElementDisplayed(notification, "Error! Notification is still displayed!");
        }
    }

    public void clickGotItButtonIfDisplayed() {
        String gotItErrorMsg = "Error! Can not find 'GOT IT' button of on-boarding dialog!";
        if (waitForElementDisplayed(gotItButton, gotItErrorMsg)) {
            waitForElement(gotItButton, gotItErrorMsg).click();
        }
    }

    public void addArticleToExistingListByNameOrCreateNew(String listName) {
        String myBookmarksError = "Error! Can not find my favorite folder!";
        if (isAndroid()) {
            ParametrizedSelector selector = TMPL_XPATH__ANDROIDBOOKMARKS_LIST_BY_TITLE_TEXT.applyParameters(listName);
            //list already exists
            if (waitForElementDisplayed(selector, myBookmarksError)) {
                waitForElement(selector, myBookmarksError).click();
            } else {
                //new list
                addNewBookmarkList(listName);
            }
        } else {
            By myBookmarks = By.id(listName);
            //list already exists
            if (waitForElementDisplayed(myBookmarks, myBookmarksError)) {
                waitForElement(myBookmarks, myBookmarksError).click();
            } else {
                //new list
                waitForElement(addToReadingListCross,
                        "Error! 'Add to reading list' cross is not displayed!").click();
                addNewBookmarkList(listName);
            }
        }
    }

    private void addNewBookmarkList(String listName) {
        WebElement input = waitForElement(newBookmarkInput,
                "Error! Can not find input element 'Name of this list'");
        input.clear();
        input.sendKeys(listName);
        waitForElement(submitNewBookmarkButton,
                "Error! Can not find on-boarding submit button!").click();
    }

    public void closeArticle() {
        waitForElement(closeArticleButton,
                "Error! can not find close article button!").click();
    }

    public void clickCloseSyncDialog() {
        waitForElement(closeSyncDialogButton, "Error! Can not find 'close' button of sync dialog!").click();
    }

    public boolean isSyncDialogDisplayed() {
        return waitForElementDisplayed(closeSyncDialogButton, "Error! Can not find 'close' button of sync dialog!");
    }

    public void tapAddArticle(String articleTitle) {
        waitForElement(TMPL_IOS_ADD_ARTICLE_ELEMENT.applyParameters(articleTitle),
                "Error! Can not find 'Add article' element!").click();
    }

    public void clickToUnsaveBookmark() {
        waitForElement(unsaveBookmarkButton,
                "Error! Unsave button is not displayed!").click();
    }
}
