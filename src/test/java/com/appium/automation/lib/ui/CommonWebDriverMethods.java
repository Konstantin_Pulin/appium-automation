package com.appium.automation.lib.ui;

import com.appium.automation.lib.ByType;
import com.appium.automation.lib.ParametrizedSelector;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.MessageFormat;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class CommonWebDriverMethods {

    @Getter
    protected RemoteWebDriver driver;

    protected String fillTemplateWithValue(String template, String value) {
        return MessageFormat.format(template, value);
    }

    protected String fillTemplateWithTwoValues(String template, String value1, String value2) {
        return MessageFormat.format(template, value1, value2);
    }

    public CommonWebDriverMethods(RemoteWebDriver driver) {
        this.driver = driver;
    }

    public WebElement waitForElement(WebElement webElement, int timeout, String errorMessage) {
        return (new WebDriverWait(driver, timeout))
                .withMessage(errorMessage)
                .until(ExpectedConditions.visibilityOf(webElement));
    }

    public WebElement waitForElement(WebElement webElement, String errorMessage) {
        return waitForElement(webElement, 15, errorMessage);
    }

    public List<WebElement> waitForElements(List<WebElement> webElements, int timeout, String errorMessage) {

        return (new WebDriverWait(driver, timeout))
                .withMessage(errorMessage)
                .until(ExpectedConditions.visibilityOfAllElements(webElements));
    }

    public List<WebElement> waitForElements(List<WebElement> webElements, String errorMessage) {
        return waitForElements(webElements, 15, errorMessage);
    }

    public WebElement waitForElement(By by, int timeout, String errorMessage) {
        return (new WebDriverWait(driver, timeout))
                .withMessage(errorMessage)
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public WebElement waitForElement(By by, String errorMessage) {
        return waitForElement(by, 15, errorMessage);
    }

    public WebElement waitForElement(ParametrizedSelector selector, int timeout, String errorMessage) {
        By by = prepareBySelector(selector);
        return (new WebDriverWait(driver, timeout))
                .withMessage(errorMessage)
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public WebElement waitForElement(ParametrizedSelector selector, String errorMessage) {
        return waitForElement(selector, 15, errorMessage);
    }

    private By prepareBySelector(ParametrizedSelector selector) {

        String query = selector.getProcessedQuery();
        ByType by = selector.getBy();

        switch (by) {
            case ByXpath:
                return new By.ByXPath(query);
            case ById:
                return new By.ById(query);
            case ByCss:
                return new By.ByCssSelector(query);
            case ByClassName:
                return new By.ByClassName(query);
            case ByName:
                return new By.ByName(query);
            case ByTagName:
                return new By.ByTagName(query);
            case ByLinkText:
                return new By.ByLinkText(query);
            case ByPartialLinkText:
                return new By.ByPartialLinkText(query);
            default:
                throw new RuntimeException("No such query type supported!");
        }
    }

    public void advancedClickToElement(WebElement element) {
        int maxTryCount = 0;
        boolean success = false;
        while (!success && maxTryCount < 10) {
            try {
                element.click();
                success = true;
            } catch (Exception e) {
                ++maxTryCount;
            }
        }

    }

    public List<WebElement> waitForElements(By by, int timeout, String errorMessage) {

        return (new WebDriverWait(driver, timeout))
                .withMessage(errorMessage)
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
    }

    public List<WebElement> waitForElements(By by, String errorMessage) {
        return waitForElements(by, 15, errorMessage);
    }

    public boolean waitForElementDisplayed(By by, String errorMessage) {
        try {
            waitForElement(by, 3, errorMessage);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean waitForElementDisplayed(WebElement webElement, String errorMessage) {
        try {
            waitForElement(webElement, 3, errorMessage);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean waitForElementDisplayed(ParametrizedSelector selector, String errorMessage) {
        try {
            waitForElement(prepareBySelector(selector), 3, errorMessage);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void assertElementPresent(By by, String errorMessage) {
        assertThat(isElementDisplayed(by))
                .as(errorMessage)
                .isTrue();
    }

    public void assertElementPresent(ParametrizedSelector selector, String errorMessage) {
        assertThat(isElementDisplayed(prepareBySelector(selector)))
                .as(errorMessage)
                .isTrue();
    }

    public boolean isElementDisplayed(By by) {
        try {
            return driver.findElement(by).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isElementDisplayed(ParametrizedSelector selector) {
        try {
            return driver.findElement(prepareBySelector(selector)).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void assertElementPresent(WebElement webElement, String errorMessage) {
        assertThat(isElementDisplayed(webElement))
                .as(errorMessage)
                .isTrue();
    }

    public boolean isElementDisplayed(WebElement webElement) {
        try {
            return webElement.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void waitElementsAreNotDisplayed(List<WebElement> webElements, int timeout, String errorMessage) {
        (new WebDriverWait(driver, timeout))
                .withMessage(errorMessage)
                .until(ExpectedConditions.invisibilityOfAllElements(webElements));
    }

    public void waitElementIsNotDisplayed(WebElement webElement, int timeout, String errorMessage) {
        (new WebDriverWait(driver, timeout))
                .withMessage(errorMessage)
                .until(ExpectedConditions.invisibilityOf(webElement));
    }

}
