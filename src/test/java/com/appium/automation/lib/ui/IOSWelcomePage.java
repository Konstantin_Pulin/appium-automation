package com.appium.automation.lib.ui;

import com.appium.automation.lib.ByType;
import com.appium.automation.lib.ParametrizedSelector;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

@Getter
public class IOSWelcomePage extends CommonWebDriverMethods {

    @iOSFindBy(id = "Next")
    private WebElement NextButton;

    @iOSFindBy(id = "Get started")
    private WebElement getStartedButton;

    //    Templates section
    private static final ParametrizedSelector TMPL_XPATH_WELCOME_TITLE_WITH_SPECIFIED_TEXT = new ParametrizedSelector("{0}", ByType.ById),
            TMPL_XPATH_WELCOME_LINK_WITH_SPECIFIED_TEXT = new ParametrizedSelector("{0}", ByType.ById);


    public IOSWelcomePage(RemoteWebDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public WebElement getTitle(String titleText) {
        return waitForElement(TMPL_XPATH_WELCOME_TITLE_WITH_SPECIFIED_TEXT.applyParameters(titleText),
                "Error! Can not find welcome page title element!");
    }

    public boolean isTitleDisplayed(String titleText) {
        return isElementDisplayed(TMPL_XPATH_WELCOME_TITLE_WITH_SPECIFIED_TEXT.applyParameters(titleText));
    }

    public WebElement getLink(String codeText) {
        return waitForElement(TMPL_XPATH_WELCOME_LINK_WITH_SPECIFIED_TEXT.applyParameters(codeText),
                "Error! Can not find welcome page link element!");
    }

}
