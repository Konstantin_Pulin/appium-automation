package com.appium.automation.lib.ui;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static com.appium.automation.lib.Platform.isAndroid;
import static com.appium.automation.lib.Platform.isIOs;

public class MainPageObject extends CommonWebDriverMethods {

    @AndroidFindBy(id = "org.wikipedia:id/icon")
    private List<WebElement> bottomNavItems;

    @FindBy(xpath = "//a[contains(@class, 'mw-ui-icon-minerva-watchlist')]")
    @iOSFindBy(id = "Saved")
    private WebElement savedArticles;

    @FindBy(id = "mw-mf-main-menu-button")
    private WebElement mvExpandMenu;

    public MainPageObject(RemoteWebDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public MainPageObject openBookmarks() {
        String errorMessage = "Error! Can not find bottom menu navigation items!";
        if (isAndroid()) {
            waitForElements(bottomNavItems,
                    errorMessage);
            bottomNavItems.get(1).click();
        } else if (isIOs()){
            waitForElement(savedArticles, errorMessage).click();
        } else {
            mvExpandMenu.click();
            advancedClickToElement(savedArticles);
        }
        return this;
    }

}
