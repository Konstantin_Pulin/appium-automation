package com.appium.automation.lib.ui;

import com.appium.automation.lib.ByType;
import com.appium.automation.lib.ParametrizedSelector;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.text.MessageFormat;
import java.util.List;

import static com.appium.automation.lib.Platform.isAndroid;
import static com.appium.automation.lib.Platform.isIOs;
import static com.appium.automation.lib.Platform.isMV;

@Getter
public class SearchPageObject extends CommonWebDriverMethods {

    @FindBy(id = "searchIcon")
    @iOSFindBy(id = "Search Wikipedia")
    @AndroidFindBy(id = "org.wikipedia:id/search_container")
    private WebElement defaultSearchInput;

    @FindBy(xpath = "//*[@name='search' and @class='search']")
    @iOSFindBy(xpath = "//XCUIElementTypeSearchField[@visible='true']")
    @AndroidFindBy(id = "org.wikipedia:id/search_src_text")
    private WebElement initializedSearchInput;

    @FindBy(xpath = "//li[contains(@class, 'page-summary')]//h3")
    @iOSFindBy(xpath = "//XCUIElementTypeCell/XCUIElementTypeLink[@visible='true']")
    @AndroidFindBy(id = "org.wikipedia:id/page_list_item_title")
    private List<WebElement> searchResultsTitles;

    @iOSFindBy(id = "clear mini")
    @AndroidFindBy(id = "org.wikipedia:id/search_close_btn")
    private WebElement clearSearchTextButton;

    //    Templates section
    private static final ParametrizedSelector TMPL_ANDROID_XPATH_ARTICLE_BY_TITLE_TEXT = new ParametrizedSelector("//*[@text=''{0}'']", ByType.ByXpath),
            TMPL_ANDROID_ARTICLE_BYTITLE_AND_DESCRIPTION_TEXT = new ParametrizedSelector("//*[@resource-id=''org.wikipedia:id/page_list_item_title'' and @text=''{0}'']/../*[@resource-id=''org.wikipedia:id/page_list_item_description'' and @text=''{1}'']", ByType.ByXpath),
            TMPL_IOS_ARTICLE_BYTITLE_AND_DESCRIPTION_TEXT = new ParametrizedSelector("//XCUIElementTypeLink[contains(@name, ''{0}'') and contains(@name, ''{1}'')]", ByType.ByXpath),
            TMPL_MV_ARTICLE_BYTITLE_AND_DESCRIPTION_TEXT = new ParametrizedSelector("//li[@title=''{0}'']//div[text()[contains(.,''{1}'')]]", ByType.ByXpath);

    public SearchPageObject(RemoteWebDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void searchForIOSArticle(String text) {
        WebElement searchInput = waitForElement(defaultSearchInput, "Wikipedia search input is not displayed!");
        searchInput.click();
        if (waitForElementDisplayed(clearSearchTextButton, "Error! Clear search input button is not displayed!")) {
            clearSearchTextButton.click();
        }
        waitForElement(initializedSearchInput,
                "Error! Can not find initialized search input!").sendKeys(text);
    }

    public void searchForArticle(String text) {
        initSearchInput();
        getSearchInput()
                .sendKeys(text);
    }

    public WebElement getSearchInput() {
        return waitForElement(initializedSearchInput,
                "Error! Wikipedia default search input with predefined text is not displayed!");
    }

    public void initSearchInput() {
        waitForElement(defaultSearchInput, "Wikipedia search input is not displayed!")
                .click();
    }

    public List<WebElement> getDisplayedSearchResults() {
        return waitForElements(getSearchResultsTitles(),
                15,
                "Error! There are no results displayed!");
    }

    public void openArticle(String title) {
        if (isAndroid()) {
            waitForElement(TMPL_ANDROID_XPATH_ARTICLE_BY_TITLE_TEXT.applyParameters(title),
                    "Error! Can not open article with specified title: " + title)
                    .click();
        } else {
            waitForElements(searchResultsTitles,
                    "Error! Search results are not displayed!").stream()
                    .filter(e -> e.getText()
                            .startsWith(title))
                    .findFirst()
                    .get().click();
        }
    }

    public String getSearchInputText() {
        return getSearchInput().getText();
    }

    public void clearSearchText() {
        waitForElement(clearSearchTextButton
                , "Error! 'Clear search request button' is not displayed!")
                .click();
    }

    public WebElement getArticleByTitleAndDescriptionText(String title, String description) {
        String errorMessage = MessageFormat.format("Error! Article with title: {0} and description: {1} was not found!",
                title, description);
        if (isAndroid()) {
            return waitForElement(TMPL_ANDROID_ARTICLE_BYTITLE_AND_DESCRIPTION_TEXT.applyParameters(title, description),
                    errorMessage);
        } else if (isIOs()) {
            return waitForElement(TMPL_IOS_ARTICLE_BYTITLE_AND_DESCRIPTION_TEXT.applyParameters(title, description),
                    errorMessage);
        } else if (isMV()) {
            return waitForElement(TMPL_MV_ARTICLE_BYTITLE_AND_DESCRIPTION_TEXT.applyParameters(title, description),
                    errorMessage);
        }
        throw new RuntimeException("Error! Compatible profile was not set up!");
    }

}
