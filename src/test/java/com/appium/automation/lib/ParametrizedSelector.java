package com.appium.automation.lib;

import lombok.AllArgsConstructor;
import lombok.Getter;

import static java.text.MessageFormat.format;

@AllArgsConstructor
public class ParametrizedSelector {

    @Getter
    private ByType by;
    private String query;
    private String processedQuery;

    public ParametrizedSelector(String query, ByType by) {
        this.query = query;
        this.by = by;
        this.processedQuery = "";
    }

    public ParametrizedSelector applyParameters(String... parameters) {
        this.processedQuery = format(query, parameters);
        return this;
    }

    public String getProcessedQuery() {
        if (processedQuery.equals("")) {
            this.processedQuery = query;
        }
        return this.processedQuery;
    }
}
