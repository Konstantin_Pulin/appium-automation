package com.appium.automation.lib;

public enum ByType {
    ByXpath,
    ByCss,
    ByLinkText,
    ByPartialLinkText,
    ByTagName,
    ByName,
    ById,
    ByClassName

}
