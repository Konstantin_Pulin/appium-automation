package com.appium.automation.steps;

import com.appium.automation.lib.ui.IOSWelcomePage;
import org.openqa.selenium.remote.RemoteWebDriver;

import static org.assertj.core.api.Assertions.assertThat;

public class IOSWelcomePageSteps extends BaseSteps {

    private IOSWelcomePage welcomePage;

    public IOSWelcomePageSteps(RemoteWebDriver driver) {
        super(driver);
        welcomePage = new IOSWelcomePage(driver);
    }


    public IOSWelcomePageSteps passWelcomePagesIfDisplayed() {
        String wikiTitle = "The free encyclopedia";
        if (welcomePage.isTitleDisplayed(wikiTitle)) {
            checkPageTitle(wikiTitle)
                    .checkPageLink("Learn more about Wikipedia")
                    .goToNextPage()
                    .checkPageTitle("New ways to explore")
                    .goToNextPage()
                    .goToNextPage()
                    .getStarted();
        }
        return this;
    }

    public IOSWelcomePageSteps checkPageTitle(String title) {
        assertThat(welcomePage.getTitle(title).isDisplayed()).isTrue();
        return this;
    }


    public IOSWelcomePageSteps checkPageLink(String linkText) {
        assertThat(welcomePage.getLink(linkText).isDisplayed()).isTrue();
        return this;
    }

    public IOSWelcomePageSteps goToNextPage() {
        welcomePage.getNextButton().click();
        return this;
    }

    public IOSWelcomePageSteps getStarted() {
        welcomePage.getGetStartedButton().click();
        return this;
    }

}
