package com.appium.automation.steps;

import com.appium.automation.lib.ui.BookmarksPageObject;
import com.appium.automation.lib.ui.MainPageObject;
import org.openqa.selenium.remote.RemoteWebDriver;

import static com.appium.automation.lib.Platform.isIOs;

public class MainPageSteps extends BaseSteps {

    private MainPageObject mainPageObject;

    public MainPageSteps(RemoteWebDriver driver) {
        super(driver);
        mainPageObject = new MainPageObject(driver);
    }

    public BaseSteps openBookmarksTab() {
        mainPageObject.openBookmarks();
        if (isIOs()) {
            new BookmarksPageObject(mainPageObject.getDriver()).getToIosReadingList();
        }
        return this;
    }

}
