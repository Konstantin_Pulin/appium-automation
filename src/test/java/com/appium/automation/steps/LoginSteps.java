package com.appium.automation.steps;

import com.appium.automation.lib.ui.LoginPageObject;
import lombok.Getter;
import org.openqa.selenium.remote.RemoteWebDriver;

public class LoginSteps extends BaseSteps {

    @Getter
    private LoginPageObject loginPageObject;

    private final static String LOGIN = "appiumCourseUser";
    private final static String PWD = "appiumCourseUser2019";

    public LoginSteps(RemoteWebDriver driver) {
        super(driver);
        loginPageObject = new LoginPageObject(driver);
    }

    public LoginSteps login() {
        loginPageObject.login(LOGIN, PWD);
        return this;
    }

}
