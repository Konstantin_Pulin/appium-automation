package com.appium.automation.steps;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.time.Duration;

public abstract class BaseSteps {

    protected RemoteWebDriver driver;

    public BaseSteps(RemoteWebDriver driver) {
        this.driver = driver;
    }

    protected BaseSteps scrollUp(int speed) {
        if (driver instanceof AppiumDriver) {
            TouchAction touchAction = new TouchAction(((AppiumDriver) driver));
            Dimension dimension = driver.manage().window().getSize();

            int xPos = dimension.width / 2;
            int fromYPos = (int) (dimension.height * 0.6);
            int toYPos = (int) (dimension.height * 0.4);

            touchAction
                    .press(PointOption.point(xPos, fromYPos))
                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(speed)))
                    .moveTo(PointOption.point(xPos, toYPos))
                    .release()
                    .perform();
        } else {
            JavascriptExecutor je = driver;
            je.executeScript("window.scrollBy(0, 250)");
        }
        return this;
    }

    protected BaseSteps scrollUpQuick() {
        scrollUp(200);
        return this;
    }

}
