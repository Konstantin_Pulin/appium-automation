package com.appium.automation.steps;

import com.appium.automation.lib.ui.BookmarksPageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.Arrays;
import java.util.List;

import static com.appium.automation.lib.Platform.isAndroid;
import static com.appium.automation.lib.Platform.isMV;
import static org.assertj.core.api.Assertions.assertThat;

public class BookmarkSteps extends BaseSteps {

    private BookmarksPageObject bookmarksPageObject;

    public BookmarkSteps(RemoteWebDriver driver) {
        super(driver);
        bookmarksPageObject = new BookmarksPageObject(driver);
    }

    public BookmarkSteps openBookmarksList(String name) {
        if (!isMV()) {
            assertThat(bookmarksPageObject.getBookmarksContainers()).isNotEmpty();
            bookmarksPageObject.openBookmarkList(name);
        }
        return this;
    }

    public BookmarkSteps assertArticlesDisplayed(String... titles) {
        if (isAndroid()) {
            assertThat(bookmarksPageObject.getFavoriteArticlesPageTitle().isDisplayed()).isTrue();
        }
        List<WebElement> storedArticles = bookmarksPageObject.getArticlesTitles();
        Arrays.stream(titles).forEach(title -> assertThat(storedArticles)
                .as("Error! No result elements contains text: " + title)
                .extracting(WebElement::getText)
                .filteredOn(e -> e.contains(title))
                .hasSize(1));
        return this;
    }

    public BookmarkSteps deleteArticle(String title) {
        bookmarksPageObject.deleteArticle(title);
        return this;
    }

    public BookmarkSteps openArticle(String title) {
        bookmarksPageObject.openArticle(title);
        return this;
    }

    public int getSavedArticlesCount() {
        return bookmarksPageObject.getArticlesTitles().size();
    }

    public BookmarkSteps assertArticlesCountAfterDeletion(int countBeforeDeletion) {
        if (isMV()) {
            driver.navigate().refresh();
        }
        List<WebElement> storedArticles = bookmarksPageObject.getArticlesTitles();
        assertThat(storedArticles.size())
                .as("Error! Article was not removed from bookmarks!")
                .isEqualTo(countBeforeDeletion - 1);
        return this;
    }
}
