package com.appium.automation.steps;

import com.appium.automation.lib.ui.ArticlePageObject;
import lombok.Getter;
import org.openqa.selenium.remote.RemoteWebDriver;

import static com.appium.automation.lib.Platform.isAndroid;
import static com.appium.automation.lib.Platform.isIOs;
import static com.appium.automation.lib.Platform.isMV;
import static org.assertj.core.api.Assertions.assertThat;

public class ArticleSteps extends BaseSteps {

    @Getter
    private ArticlePageObject articlePageObject;

    public ArticleSteps(RemoteWebDriver driver) {
        super(driver);
        articlePageObject = new ArticlePageObject(driver);
    }

    public ArticleSteps addArticleToReadingList(String listName, String articleTitle) {

        if (isAndroid()) {
            assertThat(articlePageObject.getArticleTitleElement().isDisplayed()).isTrue();
        } else if (isIOs()) {
            articlePageObject.waitIOSArticleTitleDisplayed(articleTitle);
        } else if (isMV()) {
            if (articlePageObject.isArticleSavedToList()) {
                articlePageObject.clickAddToBookmark();
            }
        }

        articlePageObject.clickAddToBookmark();

        if (isAndroid()) {
            articlePageObject.clickGotItButtonIfDisplayed();
        } else if (isIOs()) {
            if (articlePageObject.isSyncDialogDisplayed()) {
                articlePageObject.clickCloseSyncDialog();
                articlePageObject.clickToUnsaveBookmark();
                articlePageObject.clickAddToBookmark();
            }
            articlePageObject.tapAddArticle(articleTitle);
        }

        if (!isMV()) {
            articlePageObject.addArticleToExistingListByNameOrCreateNew(listName);
        }
        return this;
    }

    public ArticleSteps closeOpenedArticle() {
        if (!isMV()) {
            articlePageObject.closeArticle();
        }
        return this;
    }

    public void assertArticleTitle(String expectedArticleTitle) {
        if (!isIOs()) {
            assertThat(articlePageObject.getArticleTitleText()).isEqualTo(expectedArticleTitle);
        } else {
            articlePageObject.waitIOSArticleTitleDisplayed(expectedArticleTitle);
        }
    }

    public ArticleSteps checkOpenedArticleTitlePromptly(String articleTitle) {
        articlePageObject.checkOpenedArticleTitlePromptly(articleTitle);
        return this;
    }

}
