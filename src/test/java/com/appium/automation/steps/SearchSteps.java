package com.appium.automation.steps;

import com.appium.automation.lib.ui.SearchPageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.List;

import static com.appium.automation.lib.Platform.isAndroid;
import static com.sun.javafx.font.PrismFontFactory.isAndroid;
import static org.assertj.core.api.Assertions.assertThat;

public class SearchSteps extends BaseSteps {

    SearchPageObject searchPageObject;

    public SearchSteps(RemoteWebDriver driver) {
        super(driver);
        this.searchPageObject = new SearchPageObject(driver);
    }

    public SearchSteps searchFor(String article) {
        if (!isAndroid()) {
            IOSWelcomePageSteps iosWelcomePageSteps = new IOSWelcomePageSteps(driver);
            iosWelcomePageSteps.passWelcomePagesIfDisplayed();
            searchPageObject.searchForIOSArticle(article);
        } else {
            searchPageObject.searchForArticle(article);
        }
        return this;
    }

    public SearchSteps assertSearchResultSetContainsArticle(String title) {

        int swipeCount = 0;

        List<WebElement> searchResults = null;

        while (swipeCount < 15) {
            assertSearchResultsAreNotEmpty();
            searchResults = searchPageObject.getDisplayedSearchResults();
            if (!isSearchResultContainsExpectedArticle(searchResults, title)) {
                scrollUpQuick();
                ++swipeCount;
            } else {
                break;
            }
        }

        assertThat(searchResults)
                .as("Error! There is no article displayed with expected title: " + title)
                .extracting(WebElement::getText)
                .filteredOn(e -> e.contains(title))
                .hasSize(1);
        return this;
    }

    public SearchSteps openArticle(String title) {
        searchPageObject.openArticle(title);
        return this;
    }

    private boolean isSearchResultContainsExpectedArticle(List<WebElement> results, String title) {
        String result = results.stream()
                .map(WebElement::getText)
                .filter(e -> e.contains(title))
                .findAny()
                .orElse(null);
        return result != null;
    }

    public SearchSteps initSearch() {
        if (!isAndroid()) {
            IOSWelcomePageSteps iosWelcomePageSteps = new IOSWelcomePageSteps(driver);
            iosWelcomePageSteps.passWelcomePagesIfDisplayed();
        }
        searchPageObject.initSearchInput();
        return this;
    }

    public SearchSteps assertSearchInputText(String expectedText) {
        if (isAndroid) {
            assertThat(searchPageObject.getSearchInputText()).isEqualTo(expectedText);
        } else {
            assertThat(searchPageObject.getSearchInput().getAttribute("name")).isEqualTo(expectedText);
        }
        return this;
    }

    public SearchSteps clearSearchText() {
        searchPageObject.clearSearchText();
        return this;
    }

    public SearchSteps assertSearchResultsAreEmpty() {
        searchPageObject.waitElementsAreNotDisplayed(searchPageObject.getSearchResultsTitles(), 5,
                "Error! Search results are displayed!");
        return this;
    }

    public SearchSteps assertSearchResultsAreNotEmpty() {
        assertThat(searchPageObject.getDisplayedSearchResults()).isNotEmpty();
        return this;
    }

    public SearchSteps assertSearchResultsContainsText(String expectedText) {
        assertThat(searchPageObject.getDisplayedSearchResults())
                .as("Error! Not each result element contains text 'Java'")
                .extracting(WebElement::getText)
                .filteredOn(e -> e.contains(expectedText))
                .hasSize(searchPageObject.getDisplayedSearchResults().size());
        return this;
    }

    public SearchSteps assertArticleWithCertainTitleAndDescriptionDisplayed(String title, String description) {
        assertThat(searchPageObject.getArticleByTitleAndDescriptionText(title, description).isDisplayed()).isTrue();
        return this;
    }

}
